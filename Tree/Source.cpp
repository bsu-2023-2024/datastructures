#include <iostream>
#include <fstream>
#include "BinarySearchTree.h"

using namespace std;
int main()
{
    BinarySearchTree tree;
    tree.insert(2);
    tree.insert(1);
    tree.insert(4);
    tree.insert(9);
    tree.insert(2);
    tree.insert(11);
    tree.insert(0);
    cout << tree.search(4);
    cout << tree.getMax();
    cout << tree.getMin();
    tree.printTree();
    tree.remove(4);
    cout << tree.search(4);
    tree.printTree();
}