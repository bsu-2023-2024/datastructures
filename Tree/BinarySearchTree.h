#pragma once
#include <ostream>
using namespace std;

struct TreeNode
{
	friend std::ostream& operator << (ostream& stream, const TreeNode* node)
	{
		if (node != nullptr)
		{
			node->left != nullptr ? stream << "(" << node->left->value << ") " : stream << "(_) ";
			stream << "(" << node->value << ") ";
			node->right != nullptr ? stream << "(" << node->right->value << ")" << endl : stream << "(_)" << endl;
			return stream;
		}
	}
	TreeNode* left;
	TreeNode* right;
	int value;
	TreeNode() : left(nullptr), right(nullptr), value(0) { }
	TreeNode(int value) : left(nullptr), right(nullptr), value(value) { }
};

class BinarySearchTree
{
public:
	BinarySearchTree();
	~BinarySearchTree();
	void insert(int value);
	TreeNode* search(int value);
	TreeNode* getMin();
	TreeNode* getMax();
	void remove(int value);
	void printTree();
private:
	// recursive option requires a TreeNode, but user doesn't!!!
	// These methods could be public, but my priority was simplicity for the user
	// Because of this I've also made 'root' a private variable
	void insert(int value, TreeNode*);
	TreeNode* search(int value, TreeNode*);
	TreeNode* getMin(TreeNode*);
	TreeNode* getMax(TreeNode*);
	TreeNode* remove(int value, TreeNode*);
	void printTree(TreeNode*);
	void removeBranch(TreeNode*);

	TreeNode* root;
};