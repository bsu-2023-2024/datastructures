#include <iostream>
#include "BinarySearchTree.h"

BinarySearchTree::BinarySearchTree() : root(nullptr)
{

}

BinarySearchTree::~BinarySearchTree()
{
	removeBranch(root);
}

void BinarySearchTree::removeBranch(TreeNode* node)
{
	if (node == nullptr)
	{
		return;
	}
	removeBranch(node->left);
	removeBranch(node->right);
	delete node;
}

TreeNode* BinarySearchTree::search(int value)
{
	return search(value, root);
}

TreeNode* BinarySearchTree::search(int value, TreeNode* node)
{
	if (node == nullptr)
	{
		cout << "Element isn't found" << endl;
		return nullptr;
	}
	if (node->value == value)
	{
		return node;
	}
	return value < node->value ? search(value, node->left) : search(value, node->right);
}

TreeNode* BinarySearchTree::getMin()
{
	return getMin(root);
}

TreeNode* BinarySearchTree::getMin(TreeNode* node)
{
	if (node == nullptr)
	{
		return nullptr;
	}
	if (node->left == nullptr)
	{
		return node;
	}
	return getMin(node->left);
}

TreeNode* BinarySearchTree::getMax()
{
	return getMax(root);
}


TreeNode* BinarySearchTree::getMax(TreeNode* node)
{
	if (node == nullptr)
	{
		return nullptr;
	}
	if (node->right == nullptr)
	{
		return node;
	}
	return getMax(node->right);
}

void BinarySearchTree::insert(int value)
{
	TreeNode* node = new TreeNode(value);
	if (root == nullptr)
	{
		root = node;
		return;
	}
	insert(value, root);
}

void BinarySearchTree::insert(int value, TreeNode* node)
{
	if (value < node->value)
	{
		if (node->left == nullptr)
		{
			node->left = new TreeNode(value);
		}
		else
		{
			insert(value, node->left);
		}
	}
	else
	{
		if (node->right == nullptr)
		{
			node->right = new TreeNode(value);
		}
		else
		{
			insert(value, node->right);
		}
	}
}

void BinarySearchTree::remove(int value)
{
	root = remove(value, root);
}

TreeNode* BinarySearchTree::remove(int value, TreeNode* node)
{
	if (node == nullptr)
	{
		return nullptr;
	}
	else if (value < node->value)
	{
		node->left = remove(value, node->left);
	}
	else if (value > node->value)
	{
		node->right = remove(value, node->right);
	}
	else if (node->left != nullptr && node->right != nullptr) // 2 children
		{
			TreeNode* minInRight = getMin(node->right);
			node->value = minInRight->value;
			node->right = remove(minInRight->value, node->right);
		}

	else // 1 child
	{
		if (node->left != nullptr)
		{
			node = node->left;
		}
		else if (node->right != nullptr)
		{
			node = node->right;
		}
		else // 0 children
		{
			node = nullptr;
		}
	}
	return node;
}

void BinarySearchTree::printTree()
{
	printTree(root);
	cout << endl;
}

void BinarySearchTree::printTree(TreeNode* node)
{
	if (node == nullptr)
	{
		return;
	}
	printTree(node->left);
	std::cout << node->value << " ";
	printTree(node->right);
}