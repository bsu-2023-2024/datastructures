#include "LinkedList.h"

#include <iostream>

LinkedList::LinkedList() : head(nullptr), tail(nullptr)
{
}

LinkedList::LinkedList(int* values, int n) : head(nullptr), tail(nullptr)
{
	if (n <= 0)
	{
		return;
	}

	for (int i = n - 1; i >= 0; i--)
	{
		addFirst(values[i]);
	}
}

LinkedList::~LinkedList()
{
	clear();
}

bool LinkedList::isEmpty()
{
	return !head;
}

void LinkedList::addFirst(int value)
{
	Node* node = new Node(value);

	if (isEmpty())
	{
		head = node;
		tail = node;
		return;
	}
	if (head == tail) // 1 element
	{
		tail->previous = node;
	}

	node->next = head;
	head->previous = node;
	head = node;
}

void LinkedList::addLast(int value)
{
	Node* node = new Node(value);
	if (isEmpty())
	{
		head = node;
		tail = node;
		return;
	}
	if (head == tail) // 1 element
	{
		tail->previous = node;
	}

	node->previous = tail;
	tail->next = node;
	tail = node;
}

void LinkedList::addBeforeItem(int tag, int value)
{
	if (isEmpty())
	{
		return;
	}

	Node* current = head,
		* previous = current;

	while (current)
	{
		if (current->item == tag)
		{
			if (current == head)
			{
				addFirst(value);
			}
			else
			{
				Node* node = new Node(value);
				previous->next = node;
				node->previous = previous;
				node->next = current;
				current->previous = node;
			}
			return;
		}

		previous = current;
		current = current->next;
	}
}

void LinkedList::addAfterItem(int tag, int value)
{
	if (isEmpty())
	{
		return;
	}

	Node* current = head;

	while (current)
	{
		if (current->item == tag)
		{
			Node* node = new Node(value);
			node->next = current->next;
			Node* next = current->next;
			next->previous = node;
			current->next = node;
			node->previous = current;
			return;
		}

		current = current->next;
	}
}

void LinkedList::clear()
{
	Node* current = head;
	while (current)
	{
		head = current->next;
		delete current;
		current = head;
	}
}

void LinkedList::removeFirst()
{
	LinkedList::Node* current = head;
	head = head->next;
	head->previous = nullptr;
	delete current;
}

void LinkedList::removeLast()
{
	Node* current = tail;
	tail = tail->previous;
	tail->next = nullptr;
	delete current;
}

void LinkedList::remove(int value) {
	Node* current = head, * previous = current;
	while (current->item != value)
	{
		previous = current;
		if (current->next != nullptr)
		{
			current = current->next;
		}
		else
		{
			cout << "Element " << value << " is not in list" << endl;
			return;
		}
	}
	previous->next = current->next;
	delete current;
}

std::ostream& operator<<(ostream& stream, const LinkedList& list)
{
	LinkedList::Node* current = list.head;
	
	if (current == nullptr)
	{
		stream << "List is empty." << endl;
		return stream;
	}

	while (current)
	{
		stream << *current;
		current = current->next;
	}

	stream << endl;
	
	return stream;
}