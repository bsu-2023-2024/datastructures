#pragma once
#include <ostream>

using namespace std;

class LinkedList
{
public:
	LinkedList();
	LinkedList(int*, int);
	~LinkedList();
	bool isEmpty();
	void addFirst(int);
	void addLast(int);
	void addBeforeItem(int index, int value);
	void addAfterItem(int index, int value);
	void clear();
	void removeFirst();
	void removeLast();
	void remove(int value);
	friend std::ostream& operator << (ostream& stream, const LinkedList& node);
private:
	struct Node
	{
		Node() : next(nullptr), previous(nullptr){ }
		Node(int value) : item(value), next(nullptr), previous(nullptr) { }
		friend std::ostream& operator << (ostream& stream, const Node& node)
		{
			stream << node.item << " -> ";
			return stream;
		}
		int item{ 0 };
		Node* next;
		Node* previous;
	};
	
	Node* head;
	Node* tail;
};